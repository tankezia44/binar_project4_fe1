import './App.scss';
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import SeeAllMovie from './pages/SeeAllMovie';
import NotFound from './components/NotFound';

function App() {

  return (
    <div className='App'>

    <Router>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/SeeAllMovie' element={<SeeAllMovie />} />
        <Route path='*' element={<NotFound />} />
        </Routes>
     </Router>

      
      
    </div>
  );
}

export default App;
