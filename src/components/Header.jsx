import React from 'react';
import './header.scss';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';

const Header = () => {
  return (
    <div className="header_container">
      <img width='100%' alt='' src='https://image.tmdb.org/t/p/original/AdyJH8kDm8xT8IKTlgpEC15ny4u.jpg' />  
      <div className="detail">
        <span className="title">Doctor Strange in the Multiverse of Madness</span>
        <span className="desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iusto corrupti sequi earum quisquam doloribus quo, quaerat cumque delectus illum, omnis voluptatum repellat at possimus ad sed error sapiente suscipit. Sed?</span>
        <div className="button_watch_trailer">
          <button className="play_watch_trailer">
          <PlayArrowIcon id='play_icon'></PlayArrowIcon>
          <span>WATCH TRAILER</span>
          </button>
        </div>
      </div>
    </div>
  )
}

export default Header