import React from 'react';
import { Button } from '@mui/material';
// import SearchIcon from '@mui/icons-material/Search';

const Navbar = () => {
  return (
    <div className='navbar_container'>
        
        <h1 id='judul_web'>MovieList</h1>
        
        {/* <div className='search'>
            <form>
                <input type='text' placeholder='What do you want to watch?'></input>
                <SearchIcon id='Search_Icon'></SearchIcon>
            </form>
        </div> */}

        <div className='button_navbar_all'>
            <Button variant="outlined" id='button_login'>Login</Button>
            <Button variant="contained" id='button_register'>Register</Button>   
        </div>   
    
    </div>

    
  )
}

export default Navbar