import React from 'react';
import Navbar from '../components/Navbar';
import Header from '../components/Header';
import PopularMovie from '../components/PopularMovie';

const Home = () => {
  return (
    <div>
      <Navbar />
      <Header />
      <PopularMovie />
    </div>
  )
}

export default Home